# Hackademic: Create and Manage Your Plain-Text Academic Workflow

&copy; 2016 Kizmeta and Matthew Petty [@lodestone](https://git.io/lodestone)

LICENSE MIT


# Installation

To install or update Hackademic, open up your Terminal and type `gem install hackademic`.

```ruby
gem install hackademic
```

## Hackademic Help

`hackademic help`: could be more helpful, but essentially lists some choices. Ignore any I don't mention in this document. They may be temporary.

```ruby
> hackademic help
Commands:
  hackademic compile [FORMAT]   # Type "hackademic help compile" to see choices
  hackademic help [COMMAND]     # Describe available commands or one specific command
  hackademic install_templates  # Install latest pandoc templates
  hackademic list               # List the current projects in the Projects folder
  hackademic new [PROJECT]      # Create something new!
  hackademic setup [FOLDER]     # Setup a new Hackademic repository at [FOLDER]
  hackademic sync [subcommand]  # Synchronize!
  hackademic update_pandoc      # Update to latest pandoc and pandoc-citeproc

```

# Hackademic Commands

A List of useful Hackademic Commands

## Creating New Projects: `hackademic new project`

To create a new project, open up your Terminal, `cd` into the directory you want the project to live in (usually `~/Dropbox/Hackademic`), and type the command `hackademic new project`, followed by a space and the name of the new project.

```ruby
> cd ~/Dropbox/Hackademic
> hackademic new project <ProjectName>
```

`hackademic new project [PROJECT_NAME]`: Create a new writing project in your project folder. This command will set up the folder structure, the git repo, and add other necessary files like templates. 

## Listing Current Projects: `hackademic list`

```ruby
❯ hackademic list

  ❡ hackademic ➤➤➤

    Listing projects found in /Users/lodestone/Dropbox/Hackademic/Projects/

    PX7
    ShaneDiss
    ShaneDiss.old

```

`hackademic list`: Spit out a list of projects that hackademic knows about.


## Compiling Drafts: `hackademic compile`

`hackademic compile [OPTION]`: Compile your Main Draft into multiple formats using the included templates.  
**Note:** You should `cd` into the *Project Root Folder* before running this command in the terminal. 
 

```ruby
❯ hackademic help compile
Commands:
  hackademic compile all               # compile pdf, word doc, and latex file
  hackademic compile docx              # compile a docx
  hackademic compile help [COMMAND]    # Describe subcommands or one specific subcommand
  hackademic compile latex             # compile a latex document
  hackademic compile pdf               # compile a pdf
  hackademic compile show_pdf_command  # Show the pandoc command for pdf generation
```

`hackademic compile docx`: Compile a Word doc only

`hackademic compile latex`: Compile the LaTeX only

`hackademic compile pdf` Compile a PDF only

`hackademic compile all` will compile a complete, transculded markdown file; a .tex file; MS Word Doc; and PDF.

`hackademic compile show_pdf_command`: Show the actual command that hackademic is running so you can see what's going on, tweak parameters, etc.


## Syncing the *Notes* Folders Across Projects: `hackademic sync notes`

`hackademic sync notes`: Sync project-specific notes to the general notes folder.


This command synchronizes your project-specific notes in `~/Dropbox/Hackademic/Project/ProjectName/Resources/Notes` with your general notes folder, `~/Dropbox/Hackademic/Notes`.

It takes all the files for a given project and looks for references by name to notes in the note database (`~/Dropbox/Hackademic/Notes`). Those notes get copied over to the project. Further, any notes created in the project get pushed into the note database for happy goodness. 

The benefit here is that you can reuse notes across projects without duplicating your work. Someday, we hope this will be a full-fledged *Zettelkasten*. :-0

Someday, this will be turned into a "background job" that won't require user intervention. 

# Zotero, BetterBibTeX, and Zotfile Preferences for Hackademics

## Zotero Settings

Set Zotero's `Linked Attachment Base Directory` like so:

![Zotero Prefs - Linked Attachment Base Directory](http://z.nx.is/VwJ.png)

## BetterBib{La)TeX Settings

Download the latest version of [BetterBibLaTeX](https://zotplus.github.io/better-bibtex/) from their regularly updated [Git Repo](https://github.com/ZotPlus/zotero-better-bibtex).  

Under the `BetterBibTeX` tab of Zotero's Settings, set the `Citation Key Format` to:

    [auth:fold:lower:nopunct:alphanum]-[year]

![Better BibTeX - Citation Prefs](http://z.nx.is/e3R.png)

## Zotfile

### Zotfile Hidden Setting

In order to get Zotfile to use the stable, bibtex citekey in renaming the pdf attachments, we need to adjust a hidden setting in Firefox itself.

In the Firefox URL bar type:

`about:config`

And hit enter. You will see a warning, just click through. Seriously, Firefox is just trying to scare us. We are trained professionals. We know what we are doing.

Search the page that comes up for:

`extensions.zotfile.wildcards.user`

Double click it and paste the following JSON data in the text box.

```json
{
    "1": {
        "field": "extra",
        "operations": [
            {
                "flags": "g",
                "function": "exec",
                "regex": "bibtex: *(.*)",
                "group": 1
            }
        ]
    }
}
```

### Zotfile Preferences

Now we can adjust our Zotfile renaming preferences. Make it look like the following screenshot. For clarity, the format is: `{%1}_{%t}` (That's a 1 (one) not an I or i)

Also make sure to change the maximum length of title to `39`.


![Zotfile - Renaming Rules](http://z.nx.is/1sQ.png)


You should also set Zotfile's `General Settings` to `/Hackademic-Root-Folder/Libraries/My-Library`. See the following screenshot.


![Zotfile - General Settings](http://z.nx.is/Grx.png)


Now, **GO FORTH AND DO RESEARCH!**




