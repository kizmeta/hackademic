# CHANGELOG

## 0.3.0

* Docx generation now defaults to using LibreOffice as a way to get more control over the output via an odt template.
* Refactoring and clean-ups.

## 0.2.9

* Can now specify pandoc variables for pdf generation directly in the Config.yml
