require "rubygems"
require "yaml"
require "thor"
require 'active_support'
require_relative 'hackademic/config'
require_relative 'hackademic/sync'
require_relative 'hackademic/compile'
require_relative 'hackademic/project'
require_relative 'hackademic/setup'

# TODO: Un-spaghetti-fy this project
# TODO: Handle missing Config.yml variables nicely

class HackademicCLI < Thor

  include Thor::Actions

  HACKADEMIC_DIRECTORY = File.open("#{ENV['HOME']}/.hackademic", "r").read.chomp
  source_root "#{Pathname.new(__FILE__).dirname}"

  register Hackademic::Sync, :sync, "sync [SUBCOMMAND]", "Synchronize!"
  register Hackademic::Project, :project, "project [SUBCOMMAND]", "Work with projects!"
  register Hackademic::Setup, :setup, "setup [SUBCOMMAND]", "Set up and maintenance!"

  desc 'compile [FORMAT]', 'Type "hackademic help compile" to see choices'
  subcommand 'compile', Hackademic::Compile

end
