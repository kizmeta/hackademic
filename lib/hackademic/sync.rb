module Hackademic
  class Sync < Thor
    desc "notes", "longer desc"
    def notes
      begin

        notes_database_directory_path = "#{ENV['HOME']}/Dropbox/Hackademic/Notes"
        notes_database_files = `ls -1 #{notes_database_directory_path}`.split("\n")

        # project_directory_root_path = "/Users/lodestone/Sync/Code/overall-linking-example/001-DISS"
        # for every directory in the projects folder, do this stuff:
        # p notes_database_directory_path
        Dir.glob("#{ENV['HOME']}/Dropbox/Hackademic/Projects/*").each do |dir|

          project_directory_root_path = dir
          referenced_files = `grep -a -h -r -o -G "{{.*}}" #{project_directory_root_path}`.split("\n").map{|f| f.gsub("{{", "").gsub("}}", "") }

          referenced_files.each do |file|
            if notes_database_files.include? file
              `unison -silent #{notes_database_directory_path}/#{file} #{project_directory_root_path}/Resources/Notes/#{file}`
            end
          end

          project_notes = `ls -1 #{project_directory_root_path}/Resources/Notes`.split("\n")
          project_notes.each do |file|
            `unison -silent #{project_directory_root_path}/Resources/Notes/#{file} #{notes_database_directory_path}/#{file}`
          end
        end

      rescue => ex
        File.open("#{ENV['HOME']}/Library/Logs/hackademic-error.log", "a"){|file| file << ex }
      end

    end
  end
end
