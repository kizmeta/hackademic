module Hackademic
  class Setup < Thor

    include Thor::Actions
    HACKADEMIC_DIRECTORY = File.open("#{ENV['HOME']}/.hackademic", "r").read.chomp
    source_root "#{Pathname.new(__FILE__).dirname}"

    default_task :folder

    desc "update_pandoc", "Update to latest pandoc and pandoc-citeproc"
    def update_pandoc(args=nil)
      if !pandoc_is_current
        run "brew unlink pandoc"
        run "brew install pandoc"
        run "brew link pandoc"
        run "brew unlink pandoc-citeproc"
        run "brew install pandoc-citeproc"
        run "brew link pandoc-citeproc"
      end
    end

    desc "install_pandoc_templates", "Install latest pandoc templates"
    def install_pandoc_templates(args=nil)
      if !Dir.exists?(ENV["HOME"]+"/.pandoc/templates")
        empty_directory ENV["HOME"]+"/.pandoc"
        empty_directory ENV["HOME"]+"/.pandoc/templates"
      end
      template "../../templates/project/Resources/templates/default.asciidoc", ENV["HOME"] + "/.pandoc/templates/default.asciidoc"
      template "../../templates/project/Resources/templates/default.html5", ENV["HOME"] + "/.pandoc/templates/default.html5"
      template "../../templates/project/Resources/templates/default.latex", ENV["HOME"] + "/.pandoc/templates/default.latex"
      template "../../templates/project/Resources/templates/default.odt", ENV["HOME"] + "/.pandoc/templates/default.odt"
    end

    desc "folder [FOLDER]", "Setup a new Hackademic skeleton at [FOLDER]"
    def folder(root_directory)

      say "--------------------------------------------------------------", :cyan
      say "Hackademic Setup:", :green
      say "⫸⫸⫸ Setting up main Hackademic folder structure:", :yellow

      directory = "#{root_directory}/Hackademic"
      empty_directory(directory)
      File.open("#{ENV['HOME']}/.hackademic", "w+"){|config| config << directory }

      empty_directory "#{directory}/Libraries"
      empty_directory "#{directory}/Libraries/My-Library"
      create_file "#{directory}/Libraries/My-Library/References.bib"

      empty_directory "#{directory}/Meta"

      if system("which brew")
        run "brew update"
      else
        run 'ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"'
      end

      invoke :update_pandoc
      invoke :install_pandoc_templates

      say "⫸⫸⫸ Initializing version control (git) for Notes database:", :yellow
      empty_directory "#{directory}/Notes"
      create_file "#{directory}/Notes/SampleNote.md"
      unless File.exists? "#{directory}/Notes/.git"
        inside "#{directory}/Notes" do
          run "git init ."
          run "git add ."
          run "git commit -m 'Initial commit of Notes Database'"
        end
      end

      empty_directory "#{directory}/Projects"

      say "»»»»» Done!", :green
      say "--------------------------------------------------------------", :cyan

    end

    no_tasks {

      def pandoc_is_current
        if system("which pandoc")
          current = `pandoc --version`[/.*\n/].strip.split(" ").last
          latest = `brew info pandoc`[/\w+:\s\w+\s[\d+\.]*/].split(" ").last
          current_status = semantic_version_checker(current, latest)
          current_status
        else
          false
        end
      end

      def semantic_version_checker(current, latest)
        return true if current == latest

        latest = latest.split(".")
        current = current.split(".")

        if current.length < latest.length
          current = "#{current.join(".")}" + (".0" * (latest.length - current.length))
          current = current.split(".")
        end

        # return true if current.length > latest.length
        versionator = []
        current.each_with_index do |v, index|
          version = v.to_i
          latest_version = latest[index].to_i

          return true if version > latest_version
          versionator << (version >= latest_version)
          # versionator << (v.to_i >= latest[index].to_i)
        end
        versionator.all?{|v| v == true }
      end

    }
  end

end
