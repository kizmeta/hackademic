module Hackademic

  def self.config;
    @config ||= Config.new
  end

   class Config

    def set_config_variable(name, value)
      if value.is_a? Hash
        value.each do |key, value|
          set_config_variable("#{name}_#{key}", value)
        end
      end
      # puts "=== Setting #{name} to #{value}"
      instance_variable_set("@#{name}", ENV[name.upcase] || value)
      self.class.class_eval { attr_reader name.intern }
    end

    def initialize
      if File.exists?("Resources/Config.yml")
        raw_config = File.read("Resources/Config.yml")
        erb_config = ERB.new(raw_config).result
        settings = YAML.load(erb_config)["hackademic"]
        if settings
          settings.each { |name, value|
            set_config_variable(name, value)
          }
        end
      end

      def method_missing(method)
        ""
      end

    end

  end

end
