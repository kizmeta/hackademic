require 'pathname'

module Hackademic

  class Project < Thor

    include Thor::Actions

    # source_root "#{Pathname.new(__FILE__).dirname}/templates"
    source_root "#{Pathname.new(__FILE__).dirname}"

    desc "create [PROJECTNAME]", "Setup a new Hackademic project called [PROJECTNAME]"
    def create(project_name)
      say "--------------------------------------------------------------", :cyan
      say "⫸⫸⫸⫸ Setting up project #{project_name}!", :cyan

      project = "#{hackademic_directory}/Projects/#{project_name}"
      empty_directory(project)

      templates = "#{Pathname.new(__FILE__).dirname}/templates"

      say "»»»»» Creating *_WORKING_* directory:", :green
      empty_directory("#{project}/_WORKING_")
      template "../../templates/DRAFT.md.tt", "#{project}/_WORKING_/DRAFT.md"

      say "»»»»» Creating *Archive* directory:", :green
      empty_directory("#{project}/Archive")
      create_file "#{project}/Archive/.gitkeep"

      say "»»»»» Creating *To-Process* directory:", :green
      empty_directory("#{project}/To-Process")
      create_file "#{project}/To-Process/.gitkeep"

      say "»»»»» Creating *Project Notes* directory:", :green
      empty_directory("#{project}/Resources/Notes")
      create_file "#{project}/Resources/Notes/.gitkeep"

      say "»»»»» Creating *Resources* directory:", :green
      empty_directory("#{project}/Resources")
      template '../../templates/project/Resources/Config.yml.tt', "#{project}/Resources/Config.yml"

      empty_directory("#{project}/Resources/Figures")
      create_file "#{project}/Resources/Figures/.gitkeep"

      empty_directory("#{project}/Resources/Data")
      create_file "#{project}/Resources/Data/.gitkeep"

      install_templates(project)

      empty_directory("#{project}/Resources/csl")
      template '../../templates/apa.csl', "#{project}/Resources/csl/apa.csl"

      say "»»»»» Creating *Versions* directory:", :green
      empty_directory("#{project}/Versions")
      create_file "#{project}/Versions/.gitkeep"

      say "»»»»» Creating final files:", :green
      @project_name = project_name
      template "../../templates/README.md.tt", "#{project}/README.md"
      template '../../templates/CHANGELOG.md.tt', "#{project}/CHANGELOG.md"
      template '../../templates/TODO.txt.tt', "#{project}/TODO.txt"

      say "»»»»» Initializing version control (git) for project...", :green
      inside "#{project}" do
        run "git init .", {:capture => true}
        run "git add .", {:capture => true}
        run "git commit -m 'Initial commit of #{project_name}'", {:capture => true}
      end

      say "⫸⫸⫸⫸ DONE!", :cyan
      say "⫸⫸⫸⫸ Project created at #{project}", :cyan
      say "--------------------------------------------------------------", :cyan
    end

    def self.source_root
      File.dirname(__FILE__)
    end

    desc "install_templates [PROJECTNAME]", "installs the latest templates into a project directory"
    def install_templates(project)
      project = "#{hackademic_directory}/Projects/#{project}"
      empty_directory("#{project}/Resources/templates")
      template '../../templates/project/Resources/templates/pandoc-word-template.docx', "#{project}/Resources/templates/pandoc-word-template.docx"
      template '../../templates/project/Resources/templates/default.asciidoc', "#{project}/Resources/templates/default.asciidoc"
      template '../../templates/project/Resources/templates/default.html5', "#{project}/Resources/templates/default.html5"
      template '../../templates/project/Resources/templates/default.latex', "#{project}/Resources/templates/default.latex"
      template '../../templates/project/Resources/templates/default.odt', "#{project}/Resources/templates/default.odt"
      say "Installed latest hackademic/pandoc templates into #{project}", :blue
    end

    desc "list", "List the current projects"
    def list
      say ""
      say "  ❡ hackademic ➤➤➤", :magenta
      say ""
      say "    Listing projects found in #{hackademic_directory}/Projects/", :cyan
      say ""
      say "    " + %x[ls -1 #{hackademic_directory}/Projects].gsub!("\n","\n    "), :blue
    end

    no_tasks {

      def hackademic_directory
        @hackademic_directory ||= File.open("#{ENV['HOME']}/.hackademic", "r").read.chomp
      end
    }

  end

end
