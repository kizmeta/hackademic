module Hackademic
  class Compile < Thor

    include Thor::Actions

    desc "all", "compile pdf, word doc, and latex file"
    def all
      latex
      docx
      pdf
    end

    desc "pdf", "compile a pdf"
    def pdf
      read_draft_file
      write_master_markdown_file
      system pdf_command
      say "Writing out PDF file: #{config.output_directory}/#{config.transcluded_draft.gsub(/\..*$/, '')}.pdf"
    end

    desc "show_pdf_command", "Show the pandoc command for pdf generation"
    def show_pdf_command
      say pdf_command, :blue
    end

    desc "show_docx_command", "Show the pandoc command for docx generation"
    def show_docx_command
      say docx_command, :blue
    end

    desc "docx", "compile a docx"
    def docx
      read_draft_file
      write_master_markdown_file
      system docx_command
      say "Writing out DOCX file: #{config.output_directory}/#{config.draft_name.gsub(/\..*$/, '')}.docx"
    end

    desc "latex", "compile a latex document"
    def latex
      read_draft_file
      write_master_markdown_file
      %x[pandoc #{config.output_directory}/#{config.transcluded_draft} --from=markdown+#{pandoc_from_options.join("+")} --filter pandoc-citeproc --bibliography=#{config.bibliography} --csl=#{config.citation_style} --latex-engine=xelatex --template=#{config.latex_template} -s -o "#{latex_file}"]
      say "Writing out LaTeX file: #{config.output_directory}/#{config.draft_name.gsub(/\..*$/, '')}.tex"
    end

    no_commands {

      def latex_file
        "#{config.output_directory}/#{config.draft_name.gsub(/\..*$/,'')}.tex"
      end

      def libre_office_binary
        # TODO: Check/Test this works as intended
        config.libre_office_binary || "/Applications/LibreOffice.app/Contents/MacOS/soffice"
      end

      def add_extra_pandoc_variables(command)
        if config.pandoc_variables.is_a? Hash
          config.pandoc_variables.each do |key, val|
            command << %{ -V #{key}="#{val}" }
          end
        end
        command
      end

      def docx_command
        if File.exists?(libre_office_binary)
          command = ""
          # Use pandoc to create an ODT file"
          command << %[pandoc \
                      --template=#{config.odt_template} \
                      --from=markdown+#{pandoc_from_options.join("+")} \
                      --filter pandoc-citeproc \
                      --bibliography=#{config.bibliography} \
                      --csl=#{config.citation_style} \
                      -o temp.odt \
                      #{config.output_directory}/#{config.transcluded_draft} \
                     ]
          command = add_extra_pandoc_variables(command)
          command << " && "
          # Use LibreOffice to create a DOCX file"
          command << "#{libre_office_binary} --invisible --convert-to docx temp.odt"
          command << " && "
          # Remove the un-necessary odt file
          command << "rm temp.odt"
          command << " && "
          # Rename the temp.docx and move it into the Versions directory
          command << "cp temp.docx #{config.output_directory}/#{config.draft_name.gsub(/\..*$/,'')}.docx"
          command << " && "
          command << "rm temp.docx"
        else # Run without LibreOffice
          command = %[pandoc #{config.output_directory}/#{config.transcluded_draft} --from=markdown+#{pandoc_from_options.join("+")} --filter pandoc-citeproc --bibliography=#{config.bibliography} --csl=#{config.citation_style} --reference-docx=#{config.docx_template} -s -o "#{config.output_directory}/#{config.draft_name.gsub(/\..*$/,'')}.docx"]
          command = add_extra_pandoc_variables(command)
        end
        command.gsub(/\s+/, ' ')
      end

      def pdf_command
        cmd = %{pandoc \
          --template=#{config.pdf_template} \
          -t html5 \
          --from=markdown+#{pandoc_from_options.join("+")} \
          --bibliography=#{config.bibliography} \
          --csl=#{config.citation_style} \
          -s \
          -o "#{config.output_directory}/#{config.draft_name.gsub(/\..*$/,'')}.pdf" \
          #{config.output_directory}/#{config.transcluded_draft} \
        }
        cmd = add_extra_pandoc_variables(cmd)
        cmd.gsub(/\s+/, ' ')
      end

      def transclude(file)
        file = "#{config.drafts_dir}/#{file}" unless file[Regexp.compile("^#{config.drafts_dir}")]
        if File.exists?(file)
          contents = File.open(file, 'r').read
          contents.scan(/{{(.*)}}/).flatten.each do |f|
            original_f = f
            f = f[config.drafts_dir] ? f.gsub(config.drafts_dir, '') : "#{config.drafts_dir}/#{f}"
            HackademicCLI.new.say "...Including #{f}...", :cyan
            if File.exists?(f)
              content = File.open(f).read + "\n"
              @draft = @draft.gsub("{{#{original_f}}}", content)
              transclude(f)
            end
          end
        end
      end

      def read_draft_file
        @draft = File.open("#{config.drafts_dir}/#{config.draft_name}", "r").read
        say "...Generating the *TRANSCLUDED* source...", :blue
        transclude(config.draft_name)
      end

      def write_master_markdown_file
        File.open("#{config.output_directory}/#{config.transcluded_draft}", "w") {|f| f << @draft.to_s }
      end

      def pandoc_from_options
        %w[
          yaml_metadata_block
          raw_html
          fenced_code_blocks
          auto_identifiers
          backtick_code_blocks
          pipe_tables
          markdown_in_html_blocks
          grid_tables
          multiline_tables
          simple_tables
          header_attributes
          implicit_header_references
        ]
      end

      def config; Hackademic.config; end
    }

  end

end
