require_relative "lib/hackademic/version"

Gem::Specification.new do |s|
  s.name          = 'hackademic'
  s.version       = Hackademic::VERSION
  s.date          = '2016-02-28'
  s.summary       = "Create and manage your academic workflow"
  s.description   = "Hackademic manages your plain-text academic workflow."
  s.authors       = ["Matthew Petty"]
  s.email         = 'matt@kizmeta.com'
  s.homepage      = 'http://github.com/kizmeta/hackademic'
  s.license       = 'MIT'
  s.files         = `git ls-files`.split($/)
  s.bindir        = "bin"
  s.executables   = ["hackademic"]
  s.require_paths = ["lib"]
end
